﻿using System.ComponentModel;

namespace Tilde.EMW.Contracts.Enums.Error
{
    public enum ErrorSubCode
    {
        #region Translation errors

        [Description("Translation timed out")]
        GatewayTranslationTimedOut = 3,

        [Description("")]
        WorkerTranslationGeneric = 5,

        #endregion

        #region Language direction errors

        [Description("Language direction is not found")]
        GatewayLanguageDirectionNotFound = 6,


        #endregion

        [Description("An unexpected error occured")]
        GatewayGeneric = 8,

        [Description("Request validation failed")]
        GatewayRequestValidation = 10
    }
}
