﻿namespace Tilde.EMW.Contracts.Enums.Provider
{
    public enum Type
    {
        /// <summary>
        /// CEF eTranslation https://language-tools.ec.europa.eu/
        /// </summary>
        ETranslation = 0,

        /// <summary>
        /// Custom provider
        /// </summary>
        Custom = 1
    }
}
