﻿using System.ComponentModel.DataAnnotations;

namespace Tilde.EMW.Contracts.Models.Validators
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    sealed public class UrlValidator : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            foreach (var url in value as IEnumerable<string>)
            {
                try
                {
                    var validUrl = new Uri(url);
                    if (validUrl.Scheme != "http" && validUrl.Scheme != "https")
                    {
                        return false;
                    }
                }
                catch
                {
                    return false;
                }
            }

            return true;
        }
    }
}
