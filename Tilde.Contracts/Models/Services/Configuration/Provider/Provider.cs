﻿using System.Text.Json.Serialization;

namespace Tilde.EMW.Contracts.Models.Services.Configuration.Provider
{
    public class Provider
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("name")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public string? Name { get; set; }

        [JsonPropertyName("type")]
        public Enums.Provider.Type Type { get; set; }

        [JsonPropertyName("url")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public string? Url { get; set; }

        [JsonPropertyName("auth")]
        public Authentication.Authentication Authentication { get; set; }
    }
}
