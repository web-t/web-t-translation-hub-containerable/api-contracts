﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Tilde.EMW.Contracts.Models.Services.Configuration.Language
{
    public class Language
    {
        [MaxLength(2)]
        [JsonPropertyName("trgLang")]
        public string TargetLanguage { get; set; }

        [MaxLength(255)]
        [JsonPropertyName("domain")]
        public string Domain { get; set; }
    }
}
