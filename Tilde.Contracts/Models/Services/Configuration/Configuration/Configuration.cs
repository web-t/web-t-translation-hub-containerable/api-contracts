﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Tilde.EMW.Contracts.Models.Services.Configuration.Configuration
{
    public class Configuration
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("srcLang")]
        public string SourceLanguage { get; set; }

        [JsonPropertyName("languages")]
        public IEnumerable<Language.Language> Languages { get; set; }

        [JsonPropertyName("siteUrls")]
        public IEnumerable<string> SiteUrls { get; set; }
    }
}
