﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Tilde.EMW.Contracts.Models.Validators;

namespace Tilde.EMW.Contracts.Models.Services.Configuration.Configuration
{
    public class NewConfiguration
    {
        [Required]
        [MaxLength(255)]
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [Required]
        [MaxLength(2)]
        [JsonPropertyName("srcLang")]
        public string SourceLanguage { get; set; }

        [Required]
        [MaxLength(50)]
        [JsonPropertyName("languages")]
        public IEnumerable<Language.Language> Languages { get; set; }

        [Required]
        [MaxLength(50)]
        [UrlValidator(ErrorMessage = "Provided URL has incorrect format. Please provide correct protocol and domain")]
        [JsonPropertyName("siteUrls")]
        public IEnumerable<string> SiteUrls { get; set; }
    }
}
