﻿using System.ComponentModel.DataAnnotations;

namespace Tilde.EMW.Contracts.Models.Services.Configuration.SiteUrl
{
    public class SiteUrl
    {
        [MaxLength(255)]
        public string Url { get; set; }
    }
}
