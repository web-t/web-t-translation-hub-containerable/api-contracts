﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Tilde.EMW.Contracts.Models.Services.Configuration.Authentication
{
    public class BasicAuthentication
    {
        [MaxLength(255)]
        [JsonPropertyName("username")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public string Username { get; set; }

        [MaxLength(255)]
        [JsonPropertyName("password")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public string Password { get; set; }
    }
}
