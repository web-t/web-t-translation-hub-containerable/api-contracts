﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Tilde.EMW.Contracts.Models.Services.Configuration.Authentication
{
    public class Authentication
    {
        [JsonPropertyName("basic")]
        public BasicAuthentication? BasicAuthentication { get; set; }

        [MaxLength(255)]
        [JsonPropertyName("apiKey")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public string? ApiKey { get; set; }
    }
}
