﻿using System.Text.Json.Serialization;

namespace Tilde.EMW.Contracts.Models.Common.LanguageDirection
{
    public class LanguageDirectionsResponse
    {
        [JsonPropertyName("languageDirections")]
        public IEnumerable<LanguageDirection> LanguageDirections { get; set; }
    }
}
