﻿using System.Text.Json.Serialization;

namespace Tilde.EMW.Contracts.Models.Common.Translation
{
    /// <summary>
    /// Translated information for one translation
    /// </summary>
    public record TranslationItem
    {
        /// <summary>
        /// Translated text
        /// </summary>
        [JsonPropertyName("translation")]
        public string Translation { get; init; }
    }
}
