﻿using System.Text.Json.Serialization;

namespace Tilde.EMW.Contracts.Models.Common.Errors
{
    public record APIError
    {
        [JsonPropertyName("error")]
        public Error Error { get; init; }
    }
}
