﻿using System;

namespace Tilde.EMW.Contracts.Exceptions.TranslationSystem
{
    public class TranslationTimeoutException : TimeoutException
    {
        public TranslationTimeoutException() :
            base($"Translation request timed out")
        {

        }
        public TranslationTimeoutException(TimeSpan timeout) :
            base($"Translation request timed out in: {timeout}")
        {

        }
    }
}
