﻿using System;

namespace Tilde.EMW.Contracts.Exceptions.TranslationSystem
{
    public class LanguageDirectionNotFoundException : Exception
    {
        public LanguageDirectionNotFoundException(string language) :
            base($"Chosen language '{language}' is invalid")
        {

        }
        public LanguageDirectionNotFoundException(string domain, string sourcelanguage, string targetLanguage) :
            base($"Chosen languages '{sourcelanguage}' and '{targetLanguage}' are not supported by the domain '{domain}'")
        {

        }
    }
}
