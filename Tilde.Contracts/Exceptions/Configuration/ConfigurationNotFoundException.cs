﻿namespace Tilde.EMW.Contracts.Exceptions.Configuration
{
    public class ConfigurationNotFoundException : Exception
    {
        public ConfigurationNotFoundException(Guid id) : base($"Configuration '{id}' not found")
        {

        }
    }
}
